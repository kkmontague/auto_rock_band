import pygame
from pygame.locals import *
import serial

pygame.init()
screen = pygame.display.set_mode((640, 480))
pygame.display.set_caption('Hello World')
pygame.mouse.set_visible(1)

done = False
clock = pygame.time.Clock()
old_bin_array = 0
bin_array = 0

ser = serial.Serial('COM3', 9600)

ser.write(str(bytearray([0])))

while not done:
  clock.tick(60)

  for event in pygame.event.get():
    if event.type == QUIT:
        pygame.quit()
        sys.exit()

  press = pygame.key.get_pressed()

  if( press[K_ESCAPE] ):
    print('\nKilling Program!\n')
    done = True;

  old_bin_array = bin_array;
  bin_array = 0;

  if( press[K_1] ):
    bin_array |= 0x01
  else:
    bin_array &= ~0x01

  if( press[K_2] ):
    bin_array |= 0x02
  else:
    bin_array &= ~0x02

  if( press[K_3] ):
    bin_array |= 0x04
  else:
    bin_array &= ~0x04

  if( press[K_4] ):
    bin_array |= 0x08
  else:
    bin_array &= ~0x08

  if( press[K_5] ):
    bin_array |= 0x10
  else:
    bin_array &= ~0x10

  if( press[K_SPACE] ):
    bin_array |= 0x20
  else:
    bin_array &= ~0x20

  if bin_array != old_bin_array:
    print(str(([bin_array])))
    ser.write(str(bytearray([bin_array])))

  pygame.display.flip()

