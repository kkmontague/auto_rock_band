from SimpleCV import *
import time

cam = Camera(1)

disp = Display()

while disp.isNotDone():
  img = cam.getImage()
  img.save(disp)
  if disp.mouseLeft:
    print img[disp.mouseX, disp.mouseY]