from SimpleCV import *
import time
import serial
import argparse

variable_dictionary = {
                      'greenX':0,
                      'greenY':0,
                      'redX':0,
                      'redY':0,
                      'yellowX':0,
                      'yellowY':0,
                      'blueX':0,
                      'blueY':0,
                      'orangeX':0,
                      'orangeY':0,
                      'g_thresh':0,
                      'r_thresh':0,
                      'y_thresh':0,
                      'b_thresh':0,
                      'o_thresh':0,
                      'offset':10,
                      'sleep':0
                      }

def getXY (disp, cam, string):
  print string

  while disp.isNotDone():
    img = cam.getImage()
    img.save(disp)

    if disp.mouseLeft:
      return disp.mouseX, disp.mouseY

def modify_variables (cal_file):
  for line in cal_file:
    key = line.split(':')[0]
    value = int(line.split(':')[1])
    if key not in variable_dictionary.keys():
      print "Invalid line in file \n", line
      continue
  
    if variable_dictionary[key] != value:
      print "Updating " + key + " from " + str(variable_dictionary[key]) + " to " + str(value)
      variable_dictionary[key] = value


def get_avg_brightness(x,y,img):
  r_val = g_val = b_val = 0

  # print r_val, g_val, b_val

  x_width = 5
  y_width = 60

  offset = x_width * 2 * y_width * 2 * 3

  for x_i in range(x-x_width, x+x_width):
    for y_i in range(y-y_width, y+y_width):
      r_val += img[x_i, y_i][0]
      g_val += img[x_i, y_i][1]
      b_val += img[x_i, y_i][2]

  return (r_val + g_val + b_val) / offset

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description='Automatic Rock Band playing robot')
  parser.add_argument('-f', '--filename', dest='filename',help='input file for previously generated calibration.')
  args = parser.parse_args()


  cam = Camera(1)
  disp = Display()
  

  ser = serial.Serial('COM3', 9600)
  #ser = open("output.txt", 'w+')
  ser.write(str(bytearray([0])))


  if args.filename and os.path.isfile(args.filename):
    print args.filename
    cal_filename = args.filename
    cal_file = open(cal_filename)

    for line in cal_file:
      key = line.split(':')[0]
      value = int(line.split(':')[1])
      if key not in variable_dictionary.keys():
        print "invalid calibration file. Exiting."
        exit(1)

      variable_dictionary[key] = value

  else:
    cal_filename = "calibration.txt"

    print "Generating new calibration in calibration.txt"
    if os.path.isfile(cal_filename):
      os.remove(cal_filename)
      
    cal_file = open(cal_filename, 'w+')

    variable_dictionary['greenX'], variable_dictionary['greenY'] =  getXY(disp, cam, "Green:")
    time.sleep(.5)
    
    variable_dictionary['redX'], variable_dictionary['redY'] =  getXY(disp, cam, "Red:")
    time.sleep(.5)
    
    variable_dictionary['yellowX'], variable_dictionary['yellowY'] = getXY(disp, cam, "Yellow:")
    time.sleep(.5)
    
    variable_dictionary['blueX'], variable_dictionary['blueY'] =  getXY(disp, cam, "Blue:")
    time.sleep(.5)
    
    variable_dictionary['orangeX'], variable_dictionary['orangeY'] =  getXY(disp, cam, "Orange:")
    time.sleep(.5)

    variable_dictionary['g_thresh'] = 80
    variable_dictionary['r_thresh'] = 80
    variable_dictionary['y_thresh'] = 80
    variable_dictionary['b_thresh'] = 80
    variable_dictionary['o_thresh'] = 80

    for key in variable_dictionary.keys():
      cal_file.write(key + ':' + str(variable_dictionary[key]) + '\n')

  print "Green: ", variable_dictionary['greenX'], variable_dictionary['greenY']
  print "Red: " , variable_dictionary['redX'], variable_dictionary['redY']     
  print "Yellow: ", variable_dictionary['yellowX'], variable_dictionary['yellowY']
  print "Blue: ", variable_dictionary['blueX'], variable_dictionary['blueY']   
  print "Orange: ", variable_dictionary['orangeX'], variable_dictionary['orangeY']
  
  print "Calibration Done"
  
  bin_array = 0
  old_bin_array = 0

  while disp.isNotDone():

    cal_file.close()
    cal_file = open(cal_filename, 'r')

    modify_variables(cal_file)

    if disp.mouseRight:
      ser.write(str(bytearray([0])))
      input_hold = raw_input()
  
    img = cam.getImage()
  
    g_brightness = get_avg_brightness(variable_dictionary['greenX'], variable_dictionary['greenY'], img)
    r_brightness = get_avg_brightness(variable_dictionary['redX'], variable_dictionary['redY'], img)
    y_brightness = get_avg_brightness(variable_dictionary['yellowX'], variable_dictionary['yellowY'], img)
    b_brightness = get_avg_brightness(variable_dictionary['blueX'], variable_dictionary['blueY'], img)
    o_brightness = get_avg_brightness(variable_dictionary['orangeX'], variable_dictionary['orangeY'], img)
  
    old_bin_array = bin_array
    update = False

    debug_string = ""
  
    if g_brightness > variable_dictionary['g_thresh']:
      # print "g"
      debug_string += 'g'
      bin_array |= 0x01
      if old_bin_array != bin_array:
        update = True
    else:
      debug_string += ' '
      bin_array &= ~0x01
  
    if r_brightness > variable_dictionary['r_thresh']:
      # print "r"
      debug_string += 'r'
      bin_array |= 0x02
      if old_bin_array != bin_array:
        update = True
    else:
      debug_string += ' '
      bin_array &= ~0x02
  
    if y_brightness > variable_dictionary['y_thresh']:
      # print "y"
      debug_string += 'y'
      bin_array |= 0x04
      if old_bin_array != bin_array:
        update = True
    else:
      debug_string += ' '
      bin_array &= ~0x04
  
    if b_brightness > variable_dictionary['b_thresh']:
      # print "b"
      debug_string += 'b'
      bin_array |= 0x08
      if old_bin_array != bin_array:
        update = True
    else:
      debug_string += ' '
      bin_array &= ~0x08
  
    if o_brightness > variable_dictionary['o_thresh']:
      # print "o"
      debug_string += 'o'
      bin_array |= 0x10
      if old_bin_array != bin_array:
        update = True
    else:
      debug_string += ' '
      bin_array &= ~0x10
  
    if update == True and old_bin_array != bin_array:  

      g_brightness = get_avg_brightness(variable_dictionary['greenX'],
                                        variable_dictionary['greenY'] - variable_dictionary['offset'],
                                        img)
      r_brightness = get_avg_brightness(variable_dictionary['redX'],
                                        variable_dictionary['redY'] - variable_dictionary['offset'],
                                        img)
      y_brightness = get_avg_brightness(variable_dictionary['yellowX'],
                                        variable_dictionary['yellowY'] - variable_dictionary['offset'],
                                        img)
      b_brightness = get_avg_brightness(variable_dictionary['blueX'],
                                        variable_dictionary['blueY'] - variable_dictionary['offset'],
                                        img)
      o_brightness = get_avg_brightness(variable_dictionary['orangeX'],
                                        variable_dictionary['orangeY'] - variable_dictionary['offset'],
                                        img)
  
      if g_brightness > variable_dictionary['g_thresh']:
        debug_string += 'g'
        bin_array |= 0x01
    
      if r_brightness > variable_dictionary['r_thresh']:
        debug_string += 'r'
        bin_array |= 0x02
      
      if y_brightness > variable_dictionary['y_thresh']:
        debug_string += 'y'
        bin_array |= 0x04
      
      if b_brightness > variable_dictionary['b_thresh']:
        debug_string += 'b'
        bin_array |= 0x08
      
      if o_brightness > variable_dictionary['o_thresh']:
        debug_string += 'o'
        bin_array |= 0x10
      
      print debug_string  
  
      ser.write(str(bytearray([bin_array])))
  
      bin_array |= 0x20
  
      time.sleep(float(variable_dictionary['sleep']) / 1000)
  
      ser.write(str(bytearray([bin_array])))
  
      time.sleep(.015)
  
      bin_array &= ~0x20  

      ser.write(str(bytearray([0])))