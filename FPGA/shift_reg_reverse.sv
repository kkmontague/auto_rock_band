// Variable width shift register taken from lecture slides.
module shift_reg_reverse #(parameter width=8) (
  input                    reset_n,        //reset async active low
  input                    clk,            //input clock
  input                    data_ena,       //serial data enable
  input                    serial_data,    //serial data input
  output logic [width-1:0] parallel_data   //parallel data out
);

always @ (posedge clk, negedge reset_n)
  if(!reset_n)       parallel_data <= '0; //could not do "width'd0"
  else if (data_ena)
    parallel_data <= {serial_data, parallel_data[width-1:1]};

endmodule