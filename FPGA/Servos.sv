// Kaden Montague
// Rock band guitar controllers

module Servos (
  input           clk_50m,        // Input 50Mhz crystal oscillator
  input           reset,          // Reset Pin
  input           uart_data_in,   // serial uart data line from PC
  output          [4:0] buttons,  // Output control to button pressers
  output          strum,          // Output control to strum bar
  output          [7:0] leds      // Debugging LEDs
);

wire clk_12k;                     // 12 KHz Clock
wire clk_10k;                     // 10 KHz Clock
wire clk_153_6k;                  // 153.6 KHz Clock
logic [7:0] uart_data_from_pc;    // UART data from the PC

assign leds = uart_data_from_pc;

// soft_clock #(.CYCLE(100000)) sc_2 ( // Clock for simulation
//   .clk (clk_12k)
// );

pll pll_inst0(
  .inclk0   (clk_50m), // input clock to PLL module
  .c0       (clk_12k), // Various output clocks
  .c1       (       )
);

plluart pll_inst1(
  .inclk0   (clk_50m), // input clock to PLL module
  .c0       (clk_10k), // various output clocks
  .c1       (clk_153_6k)
);

assign strum = uart_data_from_pc[5];

assign buttons = uart_data_from_pc[4:0];

uart_receiver rec_0(
  .uart_data  (uart_data_in),         // Single bit data from PC
  .clk        (clk_153_6k  ),         // 16 * baud rate ( 16 * 9600 = 153.6k )
  .reset_n    (reset       ),         // active low reset
  .uart_data_out (uart_data_from_pc ) // parallel data from PC
);

endmodule // Servos